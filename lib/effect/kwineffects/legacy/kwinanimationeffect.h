/*
SPDX-FileCopyrightText: 2011 Thomas Lübking <thomas.luebking@web.de>
SPDX-FileCopyrightText: 2018 Vlad Zahorodnii <vlad.zahorodnii@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
#pragma once

// This header is deprecated, but installed for backwards compatibility. Do not include it anymore.
// Instead directly include the header below.
#include <kwineffects/animation_effect.h>

// Below header include is preserved for backwards compatibility.
#include <kwineffects.h>
