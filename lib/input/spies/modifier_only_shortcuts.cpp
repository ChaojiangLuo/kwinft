/*
SPDX-FileCopyrightText: 2016, 2017 Martin Gräßlin <mgraesslin@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
#include "modifier_only_shortcuts.h"

namespace KWin::input
{

modifier_only_shortcuts_spy_qobject::~modifier_only_shortcuts_spy_qobject() = default;

}
